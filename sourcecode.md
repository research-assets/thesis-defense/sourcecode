こちらは、博士論文審査における各ソースコードにアクセスするためのページです。

## データ

### 単糖のリスト
[raw file](https://gitlab.com/akihirof0005/TouCom/-/raw/master/which_ring/data)

### 単糖置換行列

[raw file](https://gitlab.com/akihirof0005/TouCom-sub/-/raw/master/kcam/similarity.txt).

[open in gitlab](https://gitlab.com/akihirof0005/TouCom-sub/-/blob/master/kcam/similarity.txt).

### ヒトの糖鎖構造
[raw](https://gitlab.com/akihirof0005/TouCom-sub/-/raw/master/kcam/all)

## 全てのソースコード

[repo1](https://gitlab.com/akihirof0005/TouCom)

[repo2](https://gitlab.com/akihirof0005/TouCom-sub)
